package org.stepsrl.java.formazione.service;

import java.util.List;

import org.stepsrl.java.formazione.domain.Product;

public interface ProductService {
	Product add(Product product);
	Product get(Long id);
	Product update(Product product);
	Long delete(Long id) throws Exception;
	List<Product> getAllProducts();
}
