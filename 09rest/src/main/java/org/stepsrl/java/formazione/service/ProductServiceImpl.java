package org.stepsrl.java.formazione.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;
import org.stepsrl.java.formazione.domain.Product;

@Service
public class ProductServiceImpl implements ProductService {

	private Map<Long, Product> products = new HashMap<Long, Product>();
	private AtomicLong generator = new AtomicLong();

	public ProductServiceImpl() {
		Product product = new Product();
		product.setName("JX1 Power Drill");
		product.setDescription("Powerful hand drill, made to perfection");
		product.setPrice(new BigDecimal(129.99));
		add(product);
	}

	@Override
	public Product add(Product product) {
		long newId = generator.incrementAndGet();
		product.setId(newId);
		products.put(newId, product);
		return product;
	}

	@Override
	public Product get(Long id) {
		return products.get(id);
	}

	@Override
	public Product update(Product product) {
		if (!products.containsKey(product.getId())) {
			System.out.println("Instead of update we insert");
		}
		products.put(product.getId(), product);
		return product;
	}

	/**
	 * @return null if no id found
	 */
	@Override
	public Long delete(Long id) throws Exception {
		Long result = null;
		Product p = products.get(id);
		if (p != null){
			products.remove(id);
			result = id;
		}
		return result;
	}

	@Override
	public List<Product> getAllProducts() {
		List<Product> result = new ArrayList<Product>(products.values());
		return result;
	}
}
