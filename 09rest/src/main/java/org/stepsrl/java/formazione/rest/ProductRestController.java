package org.stepsrl.java.formazione.rest;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stepsrl.java.formazione.domain.Product;
import org.stepsrl.java.formazione.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductRestController {
	
	final static Logger logger = Logger.getLogger(ProductRestController.class);
	
	@Autowired
	private ProductService productService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<List<Product>> getAllProduct() {
		logger.debug("Entro in get all product");
		logger.info("Entro in get all product");
		logger.error("Entro in get all product");
		logger.fatal("Entro in get all product");
		
		List<Product> res = null;
		ResponseEntity<List<Product>> result = null;
		try {

			res = productService.getAllProducts();
			result = new ResponseEntity<List<Product>>(res, HttpStatus.OK);

		} catch (Exception e) {
			result = new ResponseEntity<List<Product>>(res, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	//
	// //Test restituzione testo semplice
	// @RequestMapping(path="/test",method =
	// RequestMethod.GET,consumes=MediaType.TEXT_PLAIN_VALUE, produces =
	// MediaType.TEXT_PLAIN_VALUE)
	// public String getPlainTest() {
	// String result = "prova";
	// return result;
	// }

	@RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {
		Product res = null;
		ResponseEntity<Product> result = null;
		try {
			res = productService.get(id);
			result = new ResponseEntity<Product>(res, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity<Product>(res, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody ResponseEntity<Long> deleteProduct(@PathVariable("id") Long id) {
		Long res = null;
		ResponseEntity<Long> result = null;
		try {
			res = productService.delete(id);
			result = new ResponseEntity<Long>(res, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity<Long>(res, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}


	@RequestMapping(method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE,produces = "application/json")
	public @ResponseBody ResponseEntity<Product> updateProduct(@RequestBody Product product) {
		Product res = null;
		ResponseEntity<Product> result = null;
		try {
			res = productService.update(product);
			result = new ResponseEntity<Product>(res, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity<Product>(res, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,produces = "application/json")
	public @ResponseBody ResponseEntity<Long> addProduct(@RequestBody Product product) {
		Product servResult = null;
		Long res = null;
		ResponseEntity<Long> result = null;
		try {
			servResult = productService.add(product);
			res=servResult.getId();
			result = new ResponseEntity<Long>(res, HttpStatus.OK);
		} catch (Exception e) {
			result = new ResponseEntity<Long>(res, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	


}
