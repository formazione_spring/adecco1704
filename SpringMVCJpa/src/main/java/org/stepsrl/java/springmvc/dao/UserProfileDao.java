package org.stepsrl.java.springmvc.dao;

import java.util.List;

import org.stepsrl.java.springmvc.model.UserProfile;


public interface UserProfileDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
